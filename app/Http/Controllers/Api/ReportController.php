<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index(Request $request, Report $report)
    {
        return response()->json($report->all());
    }

    public function store(Request $request)
    {
        // create new record

    }

    public function show(Request $request, $id)
    {
        // get single record

    }

    public function update(Request $request, $id)
    {
        // update single record

    }

    public function destroy($id)
    {
        // delete single record

    }
}