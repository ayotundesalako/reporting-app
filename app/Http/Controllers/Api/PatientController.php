<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function index(Request $request, Patient $patient)
    {
        return response()->json($patient->all());
    }

    public function store(Request $request)
    {
        // create new record

    }

    public function show(Request $request, $id)
    {
        // get single record

    }

    public function update(Request $request, $id)
    {
        // update single record

    }

    public function destroy($id)
    {
        // delete single record

    }
}