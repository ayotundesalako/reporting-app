<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Permission: Resident Doctor, Consultant
     *
     * @param Request $request
     * @param User    $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, User $user)
    {
        return response()->json($user->all());
    }

    /**
     * Permisssions: Resident Doctor
     *
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|alpha',
            'last_name'  => 'required|alpha',
            'password'   => 'required|min:6',
            'email'      => 'required|email|unique:users',
            'level'      => 'required|numeric',
        ]);
        // create new record
        if ($user = (new User())->create($request->all())) {
            return response()->json($user);
        }

        return response()->json([
            "message" => "Error creating user",
        ], 417);
    }

    /**
     * Permissions: Resident Doctor, Consultant
     *
     * @param Request $request
     * @param         $id
     */
    public function show(Request $request, $id)
    {
        // get single record

    }

    public function update(Request $request, $id)
    {
        // update single record

    }

    public function destroy($id)
    {
        // delete single record

    }

    public function login(Request $request, User $user)
    {
        // do login
        $user = $user->byEmail($request->input("email"))->first();

        // validation and authenticate
        if ($user && password_verify($request->input('password'), $user->password)) {
            $api_token              = str_random(64);
            $user->api_token        = $api_token;
            $user->api_token_expiry = strtotime("+30 minutes");

            if ($user->save()) {
                return response()->json([
                    'api_token' => $api_token,
                ]);
            }
        }

        return response()->json([
            'message' => 'Authentication error',
        ], 400);
    }
}