# REPORTING APP #

### Overview ###

Reporting APP is a webservice for handling data access and certain core functions for Morbid Anatomy Reporting App.

### Prior to setting up the project, please checkout to `develop` branch ###

### Docker Setup

* Install Docker on Linux (Or Docker for Mac) on OS X
* There is a `.env.tmpl` file in the project directory. Rename a copy of the file to `.env` before you run the next step
* Run `./run-local up --build` from project repository. Add the `-d` switch to run in daemon mode. This process may take a while depending on your internet speed
* Navigate to `localhost:8050` on your web browser once the step above has finished running or test the endpoints in POSTMAN via localhost:8050/v1/{{path}}
* Navigating to `localhost:8050` in your browser should display the content below:
```
   Challenge: CODE University of Applied Sciences Admission Challenge
   App name: Morbid Anatomy Reporting App 
   Interface: API 
   Version: v1 
   Tools: PHP, Nginx, MySQL, Docker, Bash 
   Crafted by: Ayotunde Salako 
   Bitbucket URL: https://bitbucket.org/ayotundesalako/reporting-app
```
* The mysql database is setup while `./run-local up --build` is running.

### Alternative setup ###

* Install Composer
* Navigate into the product repository
* Run `composer install`
* Set DB credentials in `.env` file
* Run `php artisan migrate`
* Visit the project in your browser or test the endpoints in `POSTMAN` via `localhost/reporting-app/public/v1/{{path}}`

### DB Configuration ###

Edit the file `.env` with real data, for example:

```
   DB_CONNECTION=mysql
   DB_HOST=reporting-app-mysql
   DB_PORT=3306
   DB_DATABASE=reporting-app
   DB_USERNAME=reportingapp
   DB_PASSWORD=r3p0rt1ng
```

NOTE: Lumen won't create the database for you, this has to be done manually before you can access it if you are not using the docker setup.