<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('v1', function () use ($router) {
    return redirect('/');
});

$router->group(['prefix' => 'v1'], function () use ($router) {

    $router->post('auth/login', 'Api\UserController@login');

    $router->group(['middleware' => 'auth'], function () use ($router) {
        // Users Resources
        $router->get('users', 'Api\UserController@index');
        $router->post('users', 'Api\UserController@store');
        $router->get('user/{user}', 'Api\UserController@show');
        $router->put('user/{user}', 'Api\UserController@update');
        $router->delete('user/{user}', 'Api\UserController@destroy');

        // Patients Resources
        $router->get('patients', 'Api\PatientController@index');
        $router->post('patients', 'Api\PatientController@store');
        $router->get('patient/{user}', 'Api\PatientController@show');
        $router->put('patient/{user}', 'Api\PatientController@update');
        $router->delete('patient/{user}', 'Api\PatientController@destroy');

        // Report Resources
        $router->get('reports', 'Api\ReportController@index');
        $router->post('reports', 'Api\ReportController@store');
        $router->get('report/{user}', 'Api\ReportController@show');
        $router->put('report/{user}', 'Api\ReportController@update');
        $router->delete('report/{user}', 'Api\ReportController@destroy');
    });


});

$router->get('/', function () use ($router) {
    $viewData = [
        "challenge"     => "Challenge: CODE University of Applied Sciences Admission Challenge",
        "app_name"      => "App name: Morbid Anatomy Reporting App",
        "interface"     => "Interface: API",
        "version"       => "Version: v1",
        "tools"         => "Tools: PHP, Nginx, MySQL, Docker, Bash",
        "crafted_by"    => "Crafted by: Ayotunde Salako",
        "bitbucket_url" => "Bitbucket URL: https://bitbucket.org/ayotundesalako/reporting-app",
    ];
    return view('welcome', $viewData);
});
