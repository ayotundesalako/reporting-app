#!/bin/bash
set -e
set -x


################################################################################
# install packages
################################################################################

apt-get update -qq && apt-get install -y wget gnupg
apt-get update -qq
cat /tmp/apt-packages.txt | xargs apt-get -qq --yes --force-yes install
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
docker-php-ext-install pdo pdo_mysql soap

################################################################################
# last steps and cleaning
################################################################################

apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*