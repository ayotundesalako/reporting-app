#!/bin/bash

copy_files() {
        # Copy files to /var/www/html/payloader directory everywhere except in local
        if [ "${APP_ENV}" != "local" ]; then
            cp -r /var/www/tmp/* /var/www/html/reporting-app/
        fi
}

run_migrations() {
        # Wait for database to be up and running in local
        if [ "${APP_ENV}" == "local" ]; then
            until nc -z ${DB_HOST} ${MYSQL_PORT}; do sleep 1; echo "Waiting for DB to come up..."; done
        fi
        # Run migrations
        php artisan migrate
}

setup() {
        # Run composer install
        composer install

        # Set the right permissions for the file upload directory
        chown -R www-data:www-data /var/www/html/reporting-app/storage
}

case "$1" in
    bash )
        /bin/bash
    ;;

    start )
       copy_files
       setup
       run_migrations
       php-fpm
    ;;

    test )

    ;;
esac
