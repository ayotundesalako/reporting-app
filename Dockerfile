FROM php:7.1-fpm
LABEL Maintainer Ayotunde Salako <salakoayotunde@gmail.com>

################################################################################
## setup container
################################################################################

COPY docker/setup/* /tmp/
RUN /tmp/setup.sh

################################################################################
## setup app
################################################################################

COPY docker/scripts/entrypoint.sh /usr/local/bin/entrypoint.sh
COPY . /var/www/tmp
WORKDIR /var/www/html/reporting-app

EXPOSE 9000

################################################################################
## entrypoint
################################################################################

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
