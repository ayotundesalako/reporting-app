<?php
/**
 *
 * Description
 *
 * @package        lumen.vcom
 * @category       Source
 * @author         Michael Akanji <matscode@gmail.com> {@link http://michaelakanji.com}
 * @date           2018-03-25
 *
 */
?>
<html>
<head>
    <title>
        CODE::Morbid Anatomy Reporting App
    </title>
</head>
<body>
    {{ $challenge }} <br>
    {{ $app_name }} <br>
    {{ $interface }} <br>
    {{ $version }} <br>
    {{ $tools }} <br>
    {{ $crafted_by }} <br>
    {{ $bitbucket_url }}
</body>
</html>