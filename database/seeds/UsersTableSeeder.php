<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "first_name" => "Ayotunde",
                "last_name"  => "Salako",
                "email"      => "salakoayotunde@gmail.com",
                "level"      => "1",
                "password"   => password_hash("ayotunde", PASSWORD_BCRYPT),
                "created_at" => "2018-03-26",
            ],

            [
                "first_name" => "Paula",
                "last_name"  => "Neuber",
                "email"      => "paula.neuber@code.berlin",
                "level"      => "1",
                "password"   => password_hash("paula", PASSWORD_BCRYPT),
                "created_at" => "2018-03-26",
            ],
        ];

        DB::table('users')->insert($data);
    }
}
