<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patient_id');
            $table->string('specimen_id');
            $table->string('nature_of_specimen');
            $table->string('micro');
            $table->string('macro');
            $table->string('diagnosis');
            $table->string('diagnosed_by');
            $table->string('authorized_by');
            $table->timestamp('authorization_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
